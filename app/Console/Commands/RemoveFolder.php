<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RemoveFolder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove:dashboard
    {name : Class (singular) for example User}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove Admin Dashboard Folder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');
        $this->removeFolder($name);
        $this->removeController($name);
        $this->removeModel($name);
        $this->removeRequest($name);
    }
    private function removeModel($name)
    {
        $model_path = app_path("/Models/{$name}.php");
        if (file_exists($model_path)) {
            unlink($model_path) or die("File not exists");
        }
    }
    private function removeController($name)
    {
        $controller_path = app_path("/Http/Controllers/Admin/{$name}Controller.php");
        if (file_exists($controller_path)) {
            unlink($controller_path) or  die("File not exists");
        }
    }
    private function removeRequest($name)
    {
        $request_path = app_path("/Http/Requests/{$name}Request.php");
        if (file_exists($request_path)) {
            unlink($request_path) or die("File not exists");
        }
    }
    private function removeFolder($name)
    {
        $folder_path = resource_path("views/admin/{$name}");
        if (file_exists($folder_path)) {
            unlink("$folder_path/create.blade.php");
            unlink("$folder_path/edit.blade.php");
            unlink("$folder_path/index.blade.php");
            unlink("$folder_path/show.blade.php");
            rmdir($folder_path);
        }
    }
    private function removeMigrate($name)
    {
        $file_path = base_path("database/migrations");
    }
}