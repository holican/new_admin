<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class AdminDashboard extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:dashboard
    {name : Class (singular) for example User}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create New Admin Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');
        $this->createFolder($name);
        $this->createViews($name);
        $this->makeModel($name);
        $this->makeController($name);
        $this->makeRequest($name);
        $this->makeMigrate($name);
    }

    protected function createFolder($name)
    {
        $models = base_path('app/Models');
        if (!file_exists($models)) {
            mkdir(base_path('app/Models'), 0777, true);
        }
        $adminController = app_path('/Http/Controllers/Admin');
        if (!file_exists($adminController)) {
            mkdir(app_path('/Http/Controllers/Admin'), 0777, true);
        }
        $request = app_path('/Http/Requests');
        if (!file_exists($request)) {
            mkdir(app_path('/Http/Requests'), 0777, true);
        }
        $admin_view = resource_path('views/admin');
        if (!file_exists($admin_view)) {
            mkdir(resource_path('views/admin'), 0777, true);
        }
    }
    protected function createViews($name)
    {
        mkdir(resource_path("views/admin/{$name}"), 0777, true);
        $this->createBlade($name);
    }
    protected function getData($name)
    {
        return file_get_contents(resource_path("views/CreateFolder/{$name}.stub"));
    }
    protected function makeModel($name)
    {
        $tableName = strtolower($name)."s";
        $modelTemplate = str_replace(
            [
                '{{modelName}}',
                '{{tableName}}'
            ],
            [
                $name,
                $tableName
            ],
            $this->getData('Model')
        );
        file_put_contents(app_path("/Models/{$name}.php"), $modelTemplate);
    }
    protected function makeController($name)
    {
        $controllerTemplate = str_replace(
            [
                '{{modelName}}',
                '{{modelNamePluralLowerCase}}',
                '{{modelNameSingularLowerCase}}'
            ],
            [
                $name,
                strtolower($name),
                strtolower($name)
            ],
            $this->getData('Controller')
        );
        file_put_contents(app_path("/Http/Controllers/Admin/{$name}Controller.php"), $controllerTemplate);
    }
    protected function makeRequest($name)
    {
        $requestTemplate = str_replace(
            ['{{modelName}}'],
            [$name],
            $this->getData('Request')
        );

        file_put_contents(app_path("/Http/Requests/{$name}Request.php"), $requestTemplate);
    }
    protected function createBlade($name)
    {
        $blade_path = resource_path("views/admin/$name");
        $bladeTemplate = $this->getData('View');
        $index = file_put_contents("$blade_path/index.blade.php", $bladeTemplate);
        $create = file_put_contents("$blade_path/create.blade.php", $bladeTemplate);
        $edit = file_put_contents("$blade_path/edit.blade.php", $bladeTemplate);
        $show = file_put_contents("$blade_path/show.blade.php", $bladeTemplate);
    }
    protected function makeMigrate($name)
    {
        $migrateName = strtolower($name)."s";
        $createMigration = exec("php artisan make:migration create_{$migrateName}_table --create=$migrateName");
    }
}